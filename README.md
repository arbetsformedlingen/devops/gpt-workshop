# LLM - diskussion och demo

Vi kör lite prat och demo rörande stora språkmodeller.

## Kom igång och kör exemplen själv

1. Klona detta repo.
2. Skapa `keys/secrets.json` (basera den på `keys/secrets.json.template`).
Fyll i API-nyckel och organisations-id (OpenAI).
3. [Installera Jupyter](https://jupyter.org/install) (t ex `sudo apt install jupyter`).
4. Om du har GNU Make installerat, så starta allt med att köra `make`.
Annars kör `rm -f notebooks/test*.png ; jupyter notebook`.

Jupyter kommer skriva ut en länk på localhost som du peka din webbläsare på.

Skulle du få problem med ovanstående installationsinstruktion och
kommer på en lösning så gör gärna en MR med en justering av denna
README.


## Acknowledgements
This is my first attempt att Jupyter notebooks, and I got good help and inspiration
from [this repo](https://github.com/dkhundley/openai-api-tutorial) by [David Hundley](https://github.com/dkhundley).
